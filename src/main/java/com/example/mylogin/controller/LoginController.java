package com.example.mylogin.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.mylogin.common.utils.DesUtils;
import com.example.mylogin.common.utils.mymessagequeue.MyLogoutMessageSendClient;
import com.example.mylogin.entity.AccountMQMessage;
import com.example.mylogin.entity.TokenSInfo;
import com.example.mylogin.vo.LoginParam;
import com.example.mylogin.enums.LoginPlatformEnum;
import com.example.mylogin.enums.ResponseEnum;
import com.example.mylogin.common.exception.MyException;
import com.example.mylogin.service.LoginService;
import com.example.mylogin.service.ProductService;
import com.example.mylogin.common.utils.ResponseUtils;
import com.example.mylogin.vo.ResponseResultVO;
import com.example.mylogininterceptor.annotation.LoginVerifyAnnotation;
import com.example.mylogininterceptor.common.constants.LoginInterceptorConstant;
import com.example.mylogininterceptor.common.utils.LoginVerifyUtils;
import com.example.mylogininterceptor.vo.LoginVerifyResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * 登录处理http接口类
 */
@RestController
public class LoginController {


    private static final Logger logger =  LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private LoginService loginService;

    @Autowired
    private ProductService productService;

    /**
     * 注册接口
     * 逻辑
     *  检验参数
     *  调用loginService的注册方法
     * @param loginParam 登录参数
     * @return ResponseResultVO
     */
    @PostMapping("/regist")
    public ResponseResultVO<Long> regist(LoginParam loginParam){
        try {
            logger.info("--LoginController regist start--,loginParam:{}",loginParam);
            //参数校验
            ResponseResultVO<Long> PARAM_ERROR = checkLoginParam(loginParam,ParamCheckEnum.ACCOUNT_NAME_LOGIN);
            if (PARAM_ERROR != null) return PARAM_ERROR;
            long id = loginService.regist(loginParam);
            logger.info("--LoginController regist result--,result:{}",id);
            return ResponseUtils.ok(id);
        } catch (MyException e) {
            return ResponseUtils.myException(e);
        } catch (Exception e) {
            logger.error(String.format("--LoginController regist error--,loginParam:%s",loginParam), e);
            return ResponseUtils.error();
        }
    }

    /**
     * 账户名密码登录
     * 逻辑
     *  校验参数
     *  调用loginService的登录方法
     * @param loginParam 登录参数
     * @return ResponseResultVO
     */
    @PostMapping("/accountNameLogin")
    public ResponseResultVO<Map<String,Object>> accountNameLogin(LoginParam loginParam){
        try {
            logger.info("--LoginController accountNameLogin start--,loginParam:{}",loginParam);
            //参数校验
            ResponseResultVO<Map<String, Object>> PARAM_ERROR = checkLoginParam(loginParam,ParamCheckEnum.ACCOUNT_NAME_LOGIN);
            if (PARAM_ERROR != null) return PARAM_ERROR;
            Map<String, Object> stringObjectMap = loginService.accountNameLogin(loginParam);
            logger.info("--LoginController accountNameLogin result--,result:{}",stringObjectMap);
            return ResponseUtils.ok(stringObjectMap);
        } catch (MyException e) {
            return ResponseUtils.myException(e);
        } catch (Exception e) {
            logger.error(String.format("--LoginController accountNameLogin error--,loginParam:%s",loginParam), e);
            return ResponseUtils.error();
        }
    }

    /**
     * token校验
     * 逻辑
     *  校验参数
     *  解密参数成token对象
     *  调用loginService的校验token方法
     * @param loginParam 需要检验的参数
     * @return ResponseResultVO<Map<String,Object>>
     */
    @PostMapping("verifyToken")
    public ResponseResultVO<Map<String,Object>> verifyToken(LoginParam loginParam){
        try {
            logger.info("--LoginController verifyToken start--,loginParam:{}",loginParam);
            //参数校验
            ResponseResultVO<Map<String,Object>> PARAM_ERROR = checkLoginParam(loginParam,ParamCheckEnum.VERIFY_TOKEN);
            if (PARAM_ERROR != null) return PARAM_ERROR;
            //判断token是否存在
            Map<String, Object> stringObjectMap = loginService.verifyToken(loginParam);
            logger.info("--LoginController verifyToken result--,result:{}",stringObjectMap);
            return ResponseUtils.ok(stringObjectMap);
        } catch (MyException e) {
            return ResponseUtils.myException(e);
        } catch (Exception e) {
           logger.error(String.format("--LoginController verifyToken error--,loginParam:%s",loginParam), e);
           return ResponseUtils.error();
        }

    }

    /**
     * 退出登录
     * 逻辑
     *  拦截器校验是否登录
     *  如果登录状态，失效token
     * @return ResponseResultVO<Void>
     */
    @LoginVerifyAnnotation
    @GetMapping("logout")
    public ResponseResultVO<Void> logout(HttpServletRequest request){
        Object attribute = null;
        try {
            attribute = request.getAttribute(LoginInterceptorConstant.USER_INFO);
            logger.info("-- LoginController logout 拦截器结果 --,attribute:{}",attribute);
            LoginVerifyResult loginVerifyResult = LoginVerifyResult.parseObject(attribute);
            logger.info("-- LoginController logout 解析结果 --,loginVerifyResult:{}",loginVerifyResult);
            if(!loginVerifyResult.isSuccess())
                return ResponseUtils.newResponse(loginVerifyResult.getCode(),loginVerifyResult.getMsg());
            String tokenS = LoginVerifyUtils.getTokenS(request);
            if(StringUtils.isBlank(tokenS))
                return ResponseUtils.newResponse(ResponseEnum.PARAM_ERROR.getCode(),"获取tokenS失败");
            TokenSInfo tokenSInfo = JSONObject.parseObject(DesUtils.getDecryptString(tokenS), TokenSInfo.class);
            if(tokenSInfo == null || tokenSInfo.getTokenId() < 1)
                return ResponseUtils.newResponse(ResponseEnum.DECRYPT_TOKEN_ERROR);
            loginService.invalidLoginInformation(tokenSInfo.getAccountId(),tokenSInfo.getTokenId());
            return ResponseUtils.ok();
        } catch (Exception e) {
            String format = String.format("-- LoginController logout exception --,attribute:%s",attribute);
            logger.error(format,e);
            return ResponseUtils.error();
        }
    }

    /**
     * 校验注册登录相关的信息
     *  当相同的代码出现两次之后需要考虑是否可以提取出来公共方法
     *  相同功能或者逻辑相同的代码出现两次之后需要考虑是否可以使用设计模式替换
     * @param loginParam 登录参数
     * @param checkEnum 参数校验类型
     * @return ResponseResultVO 统一返回结果
     */
    private <T> ResponseResultVO<T> checkLoginParam(LoginParam loginParam,ParamCheckEnum checkEnum) {
        //参数校验
        if(loginParam ==null)
            return ResponseUtils.newResponse(ResponseEnum.PARAM_ERROR.getCode(),"参数为空");
        switch (checkEnum){
            case VERIFY_TOKEN:
                if(StringUtils.isBlank(loginParam.getTokenS()))
                    return ResponseUtils.newResponse(ResponseEnum.PARAM_ERROR.getCode(),"tokenS为空");
                break;
            case ACCOUNT_NAME_LOGIN:
                if(StringUtils.isBlank(loginParam.getAccountName()))
                    return ResponseUtils.newResponse(ResponseEnum.PARAM_ERROR.getCode(),"账户名为空");
                if(StringUtils.isBlank(loginParam.getPassword()))
                    return ResponseUtils.newResponse(ResponseEnum.PARAM_ERROR.getCode(), "密码为空");
                if(!LoginPlatformEnum.exist(loginParam.getPlatform()))
                    return ResponseUtils.newResponse(ResponseEnum.PARAM_ERROR.getCode(),"平台不存在");
                //此方法虽然也是为了判断，但是涉及到动态存储信息
                // 接口提供原则，越少越好，越通用越好
                // 如果返回 true或者false，肯定还需要一个根据id查询的接口
                // 但是提供了根据id查询的接口，也无妨只需要判断就可以实现是否存在的判断逻辑
                if(null == productService.getById(loginParam.getProduct()))
                    return ResponseUtils.newResponse(ResponseEnum.PARAM_ERROR.getCode(),"产品不存在");
                break;
            default:
        }
        return null;
    }

    /**
     * 参数校验类型枚举类
     */
    private enum ParamCheckEnum{
        ACCOUNT_NAME_LOGIN(1,"账号密码登录"),
        VERIFY_TOKEN(2,"token校验"),
        ;
        private int type;//检验类型
        private String desc;//校验类型名称

        ParamCheckEnum(int type, String desc) {
            this.type = type;
            this.desc = desc;
        }

        public int getType() {
            return type;
        }

        public String getDesc() {
            return desc;
        }
    }

}
