package com.example.mylogin.controller;

import com.example.mylogin.common.utils.ResponseUtils;
import com.example.mylogin.vo.ResponseResultVO;
import com.example.mylogininterceptor.annotation.LoginVerifyAnnotation;
import com.example.mylogininterceptor.common.constants.LoginInterceptorConstant;
import com.example.mylogininterceptor.vo.LoginVerifyResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * =======================================================
 * @company 技术中心-中台技术部-共享平台组
 * @className TestLoginInterceptorController
 * @description 测试登录拦截器
 * @date 2021/10/17 11:21 上午
 * @author liwenjie
 * @version 0.0.1
 * =======================================================
 */
@RestController
public class TestLoginInterceptorController {

    private final static Logger logger = LoggerFactory.getLogger(TestLoginInterceptorController.class);

    /**
     * @className TestLoginInterceptorController
     * @description 校验拦截器是否可以使用
     * @date 2021/10/17 11:24 上午
     * @return com.example.mylogin.vo.ResponseResultVO<java.lang.String>
     * @author liwenjie
     */
    @LoginVerifyAnnotation
    @PostMapping("/testLoginInterceptorController")
    public ResponseResultVO<String> testLoginInterceptorController(HttpServletRequest request){
        Object attribute = request.getAttribute(LoginInterceptorConstant.USER_INFO);
        logger.info("-- TestLoginInterceptorController testLoginInterceptorController --,attribute:{}",attribute);
        LoginVerifyResult loginVerifyResult = LoginVerifyResult.parseObject(attribute);
        if(loginVerifyResult.isSuccess()){
            Long accountId = loginVerifyResult.getAccountId();
            logger.info("-- TestLoginInterceptorController testLoginInterceptorController --,accountId:{}",accountId);
        }
        return ResponseUtils.ok(null);
    }
}
