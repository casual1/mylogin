package com.example.mylogin.controller;

import com.example.mylogin.enums.ResponseEnum;
import com.example.mylogin.common.exception.MyException;
import com.example.mylogin.service.ProductService;
import com.example.mylogin.common.utils.ResponseUtils;
import com.example.mylogin.vo.ResponseResultVO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 产品管理http接口类
 *
 */
@RestController
public class ProductController {

    private static final Logger logger =  LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    /**
     * 增加产品接口
     * @param productName 产品名称，不允许重复
     * @return ResponseResultVO
     */
    @PostMapping("/addProduct")
    public ResponseResultVO<Long> addProduct(String productName){
        try {
            if(StringUtils.isBlank(productName))
                return ResponseUtils.newResponse(ResponseEnum.PARAM_ERROR.getCode(),"产品名称为空");
            long id = productService.addProduct(productName);
            logger.info("--ProductController addProduct result--,result:{}",id);
            return ResponseUtils.ok(id);
        } catch (MyException e){
            return ResponseUtils.myException(e);
        } catch (Exception e) {
            logger.error(String.format("--ProductController addProduct--,productName:%s",productName), e);
            return ResponseUtils.error();
        }
    }



}
