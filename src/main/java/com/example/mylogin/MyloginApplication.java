package com.example.mylogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling //开启定时任务功能
@SpringBootApplication
public class MyloginApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyloginApplication.class, args);
    }

}
