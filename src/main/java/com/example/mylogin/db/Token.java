package com.example.mylogin.db;

import com.example.mylogin.vo.LoginParam;

import java.util.Date;

/**
 * 登录保存的token信息
 */
public class Token {

    /**
     * token 主键
     */
    private long id;
    /**
     * 账户id
     */
    private long accountId;
    /**
     * 登录时间
     */
    private Date loginTime;
    /**
     * 登出时间
     */
    private Date logoutTime;
    /**
     * 平台
     */
    private int platform;
    /**
     * 产品
     */
    private int product;
    /**
     * 加密的token信息
     */
    private String tokenS;
    /**
     * 数据状态 TokenStateEnum 枚举类提供
     */
    private int state;

    public Token() {
    }

    public Token(long tokenId, long accountId, Date date, String tokenS, LoginParam loginParam) {
        this.id = tokenId;
        this.accountId = accountId;
        this.loginTime = date;
        this.tokenS = tokenS;
        this.platform = loginParam.getPlatform();
        this.product = loginParam.getProduct();
    }

    @Override
    public String toString() {
        return "Token{" +
                "id=" + id +
                ", accountId=" + accountId +
                ", loginTime=" + loginTime +
                ", logoutTime=" + logoutTime +
                ", platform=" + platform +
                ", product=" + product +
                ", tokenS='" + tokenS + '\'' +
                ", state=" + state +
                '}';
    }

    public Date getLogoutTime() {
        return logoutTime;
    }

    public void setLogoutTime(Date logoutTime) {
        this.logoutTime = logoutTime;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public String getTokenS() {
        return tokenS;
    }

    public void setTokenS(String tokenS) {
        this.tokenS = tokenS;
    }

}
