package com.example.mylogin.db;

import com.example.mylogin.vo.LoginParam;

/**
 * 账户信息
 */
public class AccountInfo {

    /**
     * 主键id
     */
    private long id;
    /**
     * 用户名
     */
    private String accountName;
    /**
     * 密码
     */
    private String password;
    /**
     * 平台
     */
    private int platform;
    /**
     * 产品
     */
    private int product;

    public AccountInfo() {
    }

    public AccountInfo(String accountName, String password) {
        this.accountName = accountName;
        this.password = password;
    }

    public AccountInfo(LoginParam loginParam) {
        this.accountName = loginParam.getAccountName();
        this.password = loginParam.getPassword();
        this.platform = loginParam.getPlatform();
        this.product = loginParam.getProduct();
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "AccountInfo{" +
                "id=" + id +
                ", accountName='" + accountName + '\'' +
                ", password='" + password + '\'' +
                ", platform=" + platform +
                ", product=" + product +
                '}';
    }
}
