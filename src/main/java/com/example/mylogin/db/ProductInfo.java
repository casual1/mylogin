package com.example.mylogin.db;

/**
 * 产品信息实体
 */
public class ProductInfo {
    /**
     * 主键id
     */
    private long id;
    /**
     * 产品名称
     */
    private String name;

    public ProductInfo() {
    }

    public ProductInfo(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ProductInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
