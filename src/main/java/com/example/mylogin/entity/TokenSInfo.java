package com.example.mylogin.entity;

import java.util.Date;

/**
 * 要加密给前端的token信息
 */
public class TokenSInfo {

    /**
     * tokenId
     */
    private long tokenId;
    /**
     * 账户id
     */
    private long accountId;
    /**
     * 登录时间
     */
    private Date loginTime;


    public TokenSInfo() {
    }

    public TokenSInfo(long tokenId, long accountId, Date date) {
        this.tokenId = tokenId;
        this.accountId = accountId;
        this.loginTime = date;
    }

    public long getTokenId() {
        return tokenId;
    }

    public void setTokenId(long tokenId) {
        this.tokenId = tokenId;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }



    @Override
    public String toString() {
        return "TokenSInfo{" +
                "tokenId=" + tokenId +
                ", accountId=" + accountId +
                ", loginTime=" + loginTime +
                '}';
    }
}
