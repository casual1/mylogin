package com.example.mylogin.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.example.mylogin.db.Token;
import com.example.mylogin.enums.AccountMQMessageOperateType;

import java.util.Date;

/**
 * =======================================================
 * @className UserInfoMQMessage
 * @description 账号信息相关MQ消息
 * @author liwenjie
 * @date 2021/11/22 8:42 下午
 * =======================================================
 */
public class AccountMQMessage {

    /**
     * 账号id
     */
    private Long accountId;
    /**
     * 平台
     */
    private int platform;
    /**
     * 产品
     */
    private int product;
    /**
     * 操作类型
     */
    private Integer operateType;
    /**
     * 操作时间
     */
    private Date operateTime;



    public static AccountMQMessage build(){
        return new AccountMQMessage();
    }

    public AccountMQMessage tokenLogout(Token token){
        this.setAccountId(token.getAccountId());
        this.setPlatform(token.getPlatform());
        this.setProduct(token.getProduct());
        this.setOperateType(AccountMQMessageOperateType.LOGOUT.getOperateType());
        return this;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getOperateType() {
        return operateType;
    }

    public void setOperateType(Integer operateType) {
        this.operateType = operateType;
    }

    public Date getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "AccountMQMessage{" +
                "accountId=" + accountId +
                ", platform=" + platform +
                ", product=" + product +
                ", operateType=" + operateType +
                ", operateTime=" + operateTime +
                '}';
    }

    /**
     * 装json 不保留为null的字段
     * @return json数据
     */
    public String toJSON() {
        this.setOperateTime(new Date());
        return JSON.toJSONString(this, SerializerFeature.WriteMapNullValue);
    }
}
