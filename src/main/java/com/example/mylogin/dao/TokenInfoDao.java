package com.example.mylogin.dao;

import com.example.mylogin.db.Token;

/**
 * 登录token dao
 */
public interface TokenInfoDao {

    /**
     * 保存token信息
     * @param token token值
     * @return id
     */
    long add(Token token);

    /**
     * 获取token信息
     * @param tokenId tokenId
     * @return Token
     */
    Token getTokenById(long tokenId);

    /**
     * 根据id和state获取token信息
     * @param tokenId 主键id
     * @param state 状态
     * @return Token
     */
    Token getTokenByIdAndState(long tokenId, int state);
}
