package com.example.mylogin.dao;

import com.example.mylogin.db.AccountInfo;

/**
 * 账号信息dao
 */
public interface AccountInfoDao {

    /**
     * 根据账户名获取信息
     * @param accountName
     * @return
     */
    AccountInfo getByAccountName(String accountName);

    /**
     * 添加账户信息
     * @param accountInfo
     * @return
     */
    long add(AccountInfo accountInfo);
}
