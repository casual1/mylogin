package com.example.mylogin.dao;

import com.example.mylogin.db.ProductInfo;

/**
 * 产品信息操作 dao
 */
public interface ProductDao {
    /**
     * 根据产品名称获取产品信息
     * @param productName 产品名称
     * @return 产品信息
     */
    ProductInfo getByProductName(String productName);

    /**
     * 新增产品信息
     * @param productInfo 产品信息
     * @return 主键id
     */
    long add(ProductInfo productInfo);

    /**
     * 根据id获取产品信息
     * @param productId 产品id
     * @return 产品信息
     */
    ProductInfo getByProductId(int productId);
}
