package com.example.mylogin.dao.impl;

import com.example.mylogin.dao.TokenInfoDao;
import com.example.mylogin.db.Token;
import com.example.mylogin.enums.TokenStateEnum;
import com.example.mylogin.common.utils.cache.MyCacheUtils;
import com.example.mylogin.common.utils.SnowflakeIdWorker;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 账号信息dao
 */
@Component
public class TokenInfoDaoImpl implements TokenInfoDao {


    /**
     * 持久化存储token过程
     * 如果没有id则使用id生成器生成一个放入
     * @param token token值
     * @return tokenId
     */
    @Override
    public long add(Token token) {
        if(token.getId()<=0) {
            long id = SnowflakeIdWorker.generateTokenId();
            token.setId(id);
        }
        token.setState(TokenStateEnum.valid.getState());
        token.setLoginTime(new Date());
        //持久化数据
        MyCacheUtils.getInstance().addToken(token);
        return token.getId();
    }

    /**
     * 根据id获取token的数据
     * @param tokenId tokenId
     * @return Token
     */
    @Override
    public Token getTokenById(long tokenId) {
        return MyCacheUtils.getInstance().getTokenById(tokenId);
    }

    /**
     *
     * @param tokenId 主键id
     * @param state 状态
     * @return Token
     */
    @Override
    public Token getTokenByIdAndState(long tokenId, int state) {
        return MyCacheUtils.getInstance().getTokenByIdAndState(tokenId,state);
    }
}
