package com.example.mylogin.dao.impl;

import com.example.mylogin.common.utils.cache.MyCacheUtils;
import com.example.mylogin.dao.AccountInfoDao;
import com.example.mylogin.db.AccountInfo;
import com.example.mylogin.common.utils.SnowflakeIdWorker;
import org.springframework.stereotype.Component;

/**
 * 账号信息dao
 */
@Component
public class AccountInfoDaoImpl implements AccountInfoDao {


    /**
     * 根据账户名获取账户信息
     * 逻辑
     *  先根据账户名获取id
     *  如果id小于等于0，证明账户不存在
     *  再根据id获取账户信息
     * @param accountName 账户名
     * @return 账户信息
     */
    @Override
    public AccountInfo getByAccountName(String accountName) {
        long id = MyCacheUtils.getInstance().getAccountIdByAccountName(accountName);
        if(id<=0)
            return null;
        return MyCacheUtils.getInstance().getAccountInfoById(id);
    }

    /**
     * 增加账户信息
     * 逻辑
     *  获取唯一id
     *  添加账户信息
     * @param accountInfo 账户信息
     * @return 账户id
     */
    @Override
    public long add(AccountInfo accountInfo) {
        long ID = SnowflakeIdWorker.generateAccountId();
        accountInfo.setId(ID);
        MyCacheUtils.getInstance().addAccount(accountInfo);
        return ID;
    }
}
