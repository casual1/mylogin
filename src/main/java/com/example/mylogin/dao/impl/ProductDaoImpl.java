package com.example.mylogin.dao.impl;

import com.example.mylogin.common.utils.cache.MyCacheUtils;
import com.example.mylogin.dao.ProductDao;
import com.example.mylogin.db.ProductInfo;
import com.example.mylogin.common.utils.SelfIncreasingIdWorker;
import org.springframework.stereotype.Component;

/**
 * 产品信息操作 dao实现
 */
@Component
public class ProductDaoImpl implements ProductDao {

    /**
     * 根据产品名称获取产品信息
     * 逻辑
     *  根据名称获取主键id
     *      如果存在获取产品信息
     *      如果不存在返回空
     * @param productName 产品名称
     * @return 产品信息
     */
    @Override
    public ProductInfo getByProductName(String productName) {
        long id = MyCacheUtils.getInstance().getProductIdByProductName(productName);
        if(id<0)
            return null;
        return MyCacheUtils.getInstance().getProductById(id);
    }

    /**
     * 新增产品信息
     * 逻辑
     *  获取主键id
     *  设置主键id
     *  存入缓存
     * @param productInfo 产品信息
     * @return 产品id
     */
    @Override
    public long add(ProductInfo productInfo) {
        long productId = SelfIncreasingIdWorker.getInstance().getProductId();
        productInfo.setId(productId);
        MyCacheUtils.getInstance().addProduct(productInfo);
        return productId;
    }

    /**
     * 根据产品id获取产品信息
     * @param productId 产品id
     * @return 产品信息
     */
    @Override
    public ProductInfo getByProductId(int productId) {
        return MyCacheUtils.getInstance().getProductById(productId);
    }
}
