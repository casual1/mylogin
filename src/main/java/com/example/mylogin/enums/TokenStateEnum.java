package com.example.mylogin.enums;

/**
 * =======================================================
 *
 * @company 技术中心-中台技术部-共享平台组
 * @className TokenStateEnum
 * @description 保存Token对象的数据状态枚举类
 * @date 2021/10/6 10:13 上午
 * @author liwenjie
 * @version 0.0.1
 * =======================================================
 */
public enum TokenStateEnum {

    valid(1,"有效"),
    invalid(0,"无效"),
    ;
    private int state;
    private String desc;

    TokenStateEnum(int state, String desc) {
        this.state = state;
        this.desc = desc;
    }

    public int getState() {
        return state;
    }

    public String getDesc() {
        return desc;
    }
}
