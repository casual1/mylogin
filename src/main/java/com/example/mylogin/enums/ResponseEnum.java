package com.example.mylogin.enums;

/**
 * 接口返回结果枚举类
 */
public enum ResponseEnum {
    OK(1,"成功"),
    PARAM_ERROR(-1,"参数异常"),
    ERROR(-2,"服务异常"),


    ACCOUNT_IS_EXIST(-200,"账户名已经存在"),
    ACCOUNT_IS_NOT_EXIST(-201,"账户不存在"),
    ACCOUNT_PASSWORD_ERROR(-202,"密码输入错误"),

    PRODUCT_IS_EXIST(-300,"产品名称已经存在"),

    ENCRYPT_TOKEN_ERROR(-400,"加密token失败"),
    DECRYPT_TOKEN_ERROR(-401,"解密token失败"),
    VERIFY_TOKEN_ERROR(-402,"校验token失败"),
    VERIFY_TOKEN_EXPIRATION(-404,"token已经过期"),

    TIME_SUB_TYPE_PARAM_ERROR(-500,"时间比较类型不存在"),
    ;

    private int code;
    private String msg;

    ResponseEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
