package com.example.mylogin.enums;

/**
 * 登录平台标识
 */
public enum LoginPlatformEnum {

    H5(1,"H5登录"),
    PC_WEB(2,"PC_WEB登录"),
    APP(3,"APP登录"),
    ;
    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    LoginPlatformEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 检验参数是否在枚举类中存在
     *  返回true和false更方便判断
     *  此类存在的目的就是保存平台信息和校验，所以调用方处理怎么简单怎么来
     * @param code 平台标识参数
     * @return 是否存在
     */
    public static boolean exist(int code){
        LoginPlatformEnum[] values = LoginPlatformEnum.values();
        for (LoginPlatformEnum value : values) {
            if(value.code == code) return true;
        }
        return false;
    }
}
