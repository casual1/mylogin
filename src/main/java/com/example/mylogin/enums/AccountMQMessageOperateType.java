package com.example.mylogin.enums;

/**
 * =======================================================
 * @className AccountMQMessageOperateType
 * @description 账号信息相关MQ操作类型枚举
 * @author liwenjie
 * @date 2021/11/22 8:46 下午
 * =======================================================
 */
public enum AccountMQMessageOperateType {

    LOGOUT(1,"退出登录"),
    ;

    private int operateType;
    private String desc;

    AccountMQMessageOperateType(int operateType, String desc) {
        this.operateType = operateType;
        this.desc = desc;
    }

    public int getOperateType() {
        return operateType;
    }

    public String getDesc() {
        return desc;
    }
}
