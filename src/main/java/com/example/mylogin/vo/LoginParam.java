package com.example.mylogin.vo;

/**
 * 登录接口统一封装参数
 */
public class LoginParam {
    /**
     * 用户名
     */
    private String accountName;
    /**
     * 密码
     */
    private String password;
    /**
     * 平台
     * 默认未传，不初始化的话默认为1
     */
    private int platform = -1;
    /**
     * 产品
     * 默认未传，不初始化的话默认为1
     */
    private int product = -1;
    /**
     * token
     */
    private String tokenS;
    /**
     * 一次请求的唯一标识
     */
    private String uuid;

    public LoginParam() {
    }

    public LoginParam(String accountName, String password) {
        this.accountName = accountName;
        this.password = password;
    }

    /**
     * @param accountName 账户名
     * @return 对象
     */
    public LoginParam name(String accountName){
        this.accountName = accountName;
        return this;
    }
    /**
     * @param password 密码
     * @return 对象
     */
    public LoginParam pwd(String password){
        this.password = password;
        return this;
    }
    /**
     * @param platform 平台
     * @return 对象
     */
    public LoginParam platform(int platform){
        this.platform = platform;
        return this;
    }
    /**
     * @param product 产品
     * @return 对象
     */
    public LoginParam product(int product){
        this.product = product;
        return this;
    }
    /**
     * @param tokenS 加密的token字符串
     * @return 对象
     */
    public LoginParam tokenS(String tokenS){
        this.tokenS = tokenS;
        return this;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public String getTokenS() {
        return tokenS;
    }

    public void setTokenS(String tokenS) {
        this.tokenS = tokenS;
    }

    @Override
    public String toString() {
        return "LoginParam{" +
                "accountName='" + accountName + '\'' +
                ", password='" + password + '\'' +
                ", platform=" + platform +
                ", product=" + product +
                ", tokenS='" + tokenS + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
