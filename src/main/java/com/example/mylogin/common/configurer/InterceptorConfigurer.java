package com.example.mylogin.common.configurer;

import com.example.mylogininterceptor.interceptor.MyLoginVerifyInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * =======================================================
 * @company 技术中心-中台技术部-共享平台组
 * @className InterceptorConfigurer
 * @description 启用拦截器配置
 * @date 2021/10/17 11:30 上午
 * @author liwenjie
 * @version 0.0.1
 * =======================================================
 */
@Configuration
public class InterceptorConfigurer implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //创建拦截器类
        HandlerInterceptor myLoginVerifyInterceptor = new MyLoginVerifyInterceptor();
        registry.addInterceptor(myLoginVerifyInterceptor).addPathPatterns("/**");
        WebMvcConfigurer.super.addInterceptors(registry);
    }
}
