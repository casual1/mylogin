package com.example.mylogin.common.exception;

import com.example.mylogin.enums.ResponseEnum;
/**
 * 自定义异常
 * 拦截器
 */
public class MyException extends RuntimeException {

    /**
     * 错误码
     */
    private int code;
    /**
     * 错误提示
     */
    private String msg;

    public MyException(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public MyException(ResponseEnum responseEnum) {
        this.code = responseEnum.getCode();
        this.msg = responseEnum.getMsg();
    }

    public MyException(String msg) {
        this.code = ResponseEnum.ERROR.getCode();
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
