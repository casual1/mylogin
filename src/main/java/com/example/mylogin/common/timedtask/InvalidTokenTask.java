package com.example.mylogin.common.timedtask;

import com.example.mylogin.db.Token;
import com.example.mylogin.entity.TokenSInfo;
import com.example.mylogin.service.LoginService;
import com.example.mylogin.common.utils.Constant;
import com.example.mylogin.common.utils.DateUtils;
import com.example.mylogin.common.utils.cache.MyCacheUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * =======================================================
 *
 * @company 技术中心-中台技术部-共享平台组
 * @className InvalidTokenTask
 * @description 失效过期token的定时任务
 * @date 2021/10/6 10:57 上午
 * @author liwenjie
 * @version 0.0.1
 * =======================================================
 */
@Component
public class InvalidTokenTask {

    private final static Logger logger = LoggerFactory.getLogger(InvalidTokenTask.class);


    @Autowired
    private LoginService loginService;

    /**
     * 上一次任务是否结束的标识和锁 默认已经结束
     */
    private static volatile boolean isRunning = false;

    /**
     * 获取最老的token令牌判断是否已经过期
     * 如果过期继续下一个，直到没有过期的存在
     */
    @Scheduled(cron = "*/20 * * * * ?")
    public void execute2() {
        try {
            //判断上一次任务是否结束
            if(isRunning){
                synchronized (InvalidTokenTask.class){
                    if(isRunning){
                        return;
                    }
                }
            }
            //修改标识
            isRunning = true;
            TokenSInfo firstTreeMapVerifyToken = MyCacheUtils.getInstance().getFirstTreeMapVerifyToken();
            if(firstTreeMapVerifyToken==null){
                //结束方法
                return;
            }
            //存在开始判断是否需要注销
            //循环判断登录是否过期
            while (DateUtils.compareTwoDateGteTheSub(new Date(),firstTreeMapVerifyToken.getLoginTime(), DateUtils.getTimeSubTypeEnumByType(Constant.TOKEN_EXPIRATION_TIME_TYPE),Constant.TOKEN_EXPIRATION_TIME)){
                //失效登录信息
                loginService.invalidLoginInformation(firstTreeMapVerifyToken.getAccountId(), firstTreeMapVerifyToken.getTokenId());
                //继续获取下一个数据
                firstTreeMapVerifyToken = MyCacheUtils.getInstance().getFirstTreeMapVerifyToken();
            }
            //修改标识
            isRunning = false;
        } catch (Exception e) {
            logger.error("InvalidTokenTask WorkTask execute2 exception",e);
        }
    }


    /**
     * 定期遍历所有的token数据，根据登录时间判断是否需要失效此登录信息
     *  失效信息的操作：删除缓存token，修改持久化的token状态
     * 可能存在的问题
     *  如果token数量庞大，处理一次的时间会非常缓慢，可能会出现第一个任务没有结束，第二个任务又重新开始，极限情况会造成阻塞
     *     解决方案1，将所有的token分成若干份，每份之间不重复，启动多个线程处理不同的数据（增加单次的处理速度）
     *     解决方案2，在1的基础上设置标志变量，如果上一次的任务没有执行结束则放弃本次执行，等待本次结束再次启动（防止并发执行任务）
     * 还会存在的问题
     *   现在大多数是分布式的服务，方案2的标志变量应该存在数据库或者redis中，进行缓存共享
     *
     */
//    @Scheduled(cron = "*/20 * * * * ?")
    public void execute() {
        try {
            //判断上一次任务是否结束
            if(isRunning){
                synchronized (InvalidTokenTask.class){
                    if(isRunning){
                        return;
                    }
                }
            }
            //修改标识
            isRunning = true;
            //获取所有的缓存token
            Map<Long, Map<Long, Token>> allVerifyToken = MyCacheUtils.getInstance().getAllVerifyToken();
            Set<Map.Entry<Long, Map<Long, Token>>> entries = allVerifyToken.entrySet();
            int size = entries.size();
            if(size==0){
                return;
            }
            //固定四个线程，最多5个 获取每个线程需要处理的数据个数
            int index = size / 4;
            logger.info(" InvalidTokenTask execute ...，index:{}", index);
            //保存所有线程需要处理的数据
            List<List<Map<Long, Token>>> lists = new LinkedList<>();
            //保存单个线程需要处理的数据
            List<Map<Long, Token>> workTaskParam = new LinkedList<>();
            int i = 0;
            for (Map.Entry<Long, Map<Long, Token>> entry : entries) {
                if(entry.getValue() == null){
                    continue;
                }
                workTaskParam.add(entry.getValue());
                i++;
                if(i % index  == 0) {
                    //达到条件启动线程开始处理
                    logger.info(" InvalidTokenTask WorkTask execute start ...,i:{}",i);
                    lists.add(workTaskParam);
                    workTaskParam = new LinkedList<>();
                }
            }
            //处理最后一个任务的数据
            if(!workTaskParam.isEmpty()) {
                logger.info(" InvalidTokenTask WorkTask execute start ...,i:{}", i);
                lists.add(workTaskParam);
            }
            //开始启动线程
            CountDownLatch latch = new CountDownLatch(lists.size());
            for (List<Map<Long, Token>> list : lists) {
                new Thread(new WorkTask(list, loginService, latch)).start();
            }
            latch.await();
            //修改标识
            isRunning = false;
        } catch (Exception e) {
            logger.error("InvalidTokenTask WorkTask execute exception",e);
        }
    }



}

/**
 * =======================================================
 *
 * @company 技术中心-中台技术部-共享平台组
 * @className InvalidTokenTask
 * @description 真正判断token是否过期的任务，如果过期直接失效token
 * @date 2021/10/6 8:35 下午
 * @author liwenjie
 * @version 0.0.1
 * =======================================================
 */
class WorkTask implements Runnable{

    private final static Logger logger = LoggerFactory.getLogger(WorkTask.class);

    private List<Map<Long, Token>> workTaskParam;
    private LoginService loginService;
    private CountDownLatch latch;

    public WorkTask(List<Map<Long, Token>> workTaskParam, LoginService loginService, CountDownLatch latch) {
        this.workTaskParam = workTaskParam;
        this.loginService = loginService;
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            if(workTaskParam.isEmpty()){
                return;
            }
            for (Map<Long, Token> longTokenMap : workTaskParam) {
                for (Map.Entry<Long, Token> longTokenEntry : longTokenMap.entrySet()) {
                    Token value = longTokenEntry.getValue();
                    if(value == null){
                        continue;
                    }
                    //判断登录是否过期
                    if(DateUtils.compareTwoDateGteTheSub(new Date(),value.getLoginTime(), DateUtils.getTimeSubTypeEnumByType(Constant.TOKEN_EXPIRATION_TIME_TYPE),Constant.TOKEN_EXPIRATION_TIME)){
                        //失效登录信息
                        loginService.invalidLoginInformation(value.getAccountId(), value.getId());
                    }
                }
            }
        } catch (Exception e) {
            logger.error("-- WorkTask run exception --", e);
        } finally {
            latch.countDown();
        }
    }
}
