package com.example.mylogin.common.utils.cache;

import com.example.mylogin.db.AccountInfo;
import com.example.mylogin.db.ProductInfo;
import com.example.mylogin.db.Token;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 信息存放处
 *  会有并发插入操作，缓存设置为 ConcurrentHashMap
 * protected 只能被子类操作
 */
public class MyCache {

    /**
     * id做key 账号信息做value 类似于数据库主键
     */
    protected static final Map<Long, AccountInfo> accountInfoMap = new ConcurrentHashMap<>();

    /**
     * accountName做key id做value 类似于数据库唯一主键
     */
    protected static final Map<String,Long> accountNameIdCache = new ConcurrentHashMap<>();

    /**
     * tokenid做key token做value 保存所有生成的token信息
     */
    protected static final Map<Long,Token> tokenCache = new ConcurrentHashMap<>();

    /**
     * 首先账户id做key，保存每个用户的登录token，然后tokenId做key（id生成器可以保证不会重复），保存用户每个时刻不同的登录状态，校验token时使用
     */
    protected static final Map<Long,Map<Long, Token>> verifyTokenCache = new ConcurrentHashMap<>();

    /**
     * 登录时间做key 令牌做value
     */
    protected static final Map<Date,String> treeMapVerifyTokenCache = new ConcurrentHashMap<>();

    /**
     * id做key 产品信息做value
     */
    protected static final Map<Long, ProductInfo> productInfoMap = new ConcurrentHashMap<>();

    /**
     * productName做key id做value
     */
    protected static final Map<String,Long> productNameIdCache = new ConcurrentHashMap<>();

}
