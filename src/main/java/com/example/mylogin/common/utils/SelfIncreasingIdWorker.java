package com.example.mylogin.common.utils;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 自增id生成类
 * 内部类既可以满足单例的安全性，又可以实现懒加载
 */
public class SelfIncreasingIdWorker {
    /**
     * 私有化构造器
     */
    private SelfIncreasingIdWorker(){}

    /**
     * Atomic下的类可以实现并发安全的 i++
     * token id
     */
    private AtomicLong tokenAtomicLong = new AtomicLong(1L);
    /**
     * 产品信息id
     */
    private AtomicLong productAtomicLong = new AtomicLong(1L);
    /**
     * 获取token自增id
     * @return token的主键
     */
    public long getTokenId() {
        return tokenAtomicLong.getAndIncrement();
    }
    /**
     * 获取产品信息自增id
     * @return 产品信息n的主键
     */
    public long getProductId() {
        return tokenAtomicLong.getAndIncrement();
    }

    /**
     * 内部类实现单例
     */
    private static class Instance{
        private static final SelfIncreasingIdWorker selfIncreasingIdWorker = new SelfIncreasingIdWorker();
    }
    /**
     * 单例
     * @return 自增主键工具类对象
     */
    public static SelfIncreasingIdWorker getInstance(){
        return Instance.selfIncreasingIdWorker;
    }



}
