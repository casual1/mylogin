package com.example.mylogin.common.utils.mymessagequeue;


import com.example.mylogin.common.utils.mymessagequeue.queues.MyMessageQueue;
import com.example.mylogin.common.utils.mymessagequeue.queues.MyMessageQueueUtils;
import com.example.mylogin.common.utils.mymessagequeue.queues.ReceiveMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * =======================================================
 *
 * @author liwenjie
 * @className MyReceiveClient
 * @description 接收客户端
 * @date 2021/11/20 10:44 下午
 * =======================================================
 */
public class MyLogoutMessageReceiveClient implements ReceiveMessage {

    private final static Logger logger = LoggerFactory.getLogger(MyLogoutMessageReceiveClient.class);

    private static final String topic = "user_logout";
    private static final Integer clientId = 1;


    //  在执行方法前初始化
    static {
        logger.info("-- MyLogoutMessageReceiveClient static initializer --");
        MyMessageQueueUtils.getInstance().regist(topic, MyMessageQueue.ClientType.RECEIVE.getType(), clientId);
    }

    public static void receive() {
        try {
            MyMessageQueueUtils.getInstance().receiveMessage(topic, clientId, new MyLogoutMessageReceiveClient());
        } catch (Exception e) {
            String format = String.format("-- MyLogoutMessageReceiveClient receive exception --,topic:%s,clientId:%s", topic, clientId);
            logger.error(format, e);
        }
    }


    /**
     * 处理消息的方法
     *
     * @param message 消息
     */
    @Override
    public void receiveMessage(String message) {
        logger.info("-- MyLogoutMessageReceiveClient receiveMessage --,message:{}", message);
    }
}
