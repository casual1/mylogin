package com.example.mylogin.common.utils;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

import java.net.Inet4Address;
import java.net.UnknownHostException;

/**
 * id生成器工具类
 * 网址：https://blog.csdn.net/u012488504/article/details/82194495
 *
 */
public class SnowflakeIdWorker {


    /**  开始时间截 (2015-01-01) 与现在时间戳求差值 */
    private final long twepoch = 1489111610226L;

    /** 机器id所占的位数 */
    private final long workerIdBits = 5L;

    /** 数据标识id所占的位数 */
    private final long dataCenterIdBits = 5L;

    /** 支持的最大机器id，结果是31 (这个移位算法可以很快的计算出几位二进制数所能表示的最大十进制数) */
    private final long maxWorkerId = ~(-1L << workerIdBits);

    /** 支持的最大数据标识id，结果是31 */
    private final long maxDataCenterId = ~(-1L << dataCenterIdBits);

    /** 序列在id中占的位数 */
    private final long sequenceBits = 12L;

    /** 机器ID向左移12位 */
    private final long workerIdShift = sequenceBits;

    /** 数据标识id向左移17位(12+5) */
    private final long dataCenterIdShift = sequenceBits + workerIdBits;

    /** 时间截向左移22位(5+5+12) */
    private final long timestampLeftShift = sequenceBits + workerIdBits + dataCenterIdBits;

    /** 生成序列的掩码，这里为4095 (0b111111111111=0xfff=4095) */
    private final long sequenceMask = ~(-1L << sequenceBits);

    /** 工作机器ID(0~31) */
    private long workerId;

    /** 数据中心ID(0~31) */
    private long dataCenterId;

    /** 毫秒内序列(0~4095) */
    private long sequence = 0L;

    /** 上次生成ID的时间截 */
    private long lastTimestamp = -1L;

    //保证唯一对象，如果有多个可以使用池子的思想，每次分发到不同的 idWorker 上
    //账户信息的id生成器
    private static SnowflakeIdWorker idWorkerForAccount;
    //Token信息的id生成器
    private static SnowflakeIdWorker idWorkerForToken;

    //静态方法初始化id生成对象，保证唯一性
    static {
        idWorkerForAccount = new SnowflakeIdWorker(getWorkId(), getDataCenterId());
        idWorkerForToken = new SnowflakeIdWorker(getWorkId(), getDataCenterId());
    }

    /**
     * 静态工具类
     * 获取账户id
     *
     * @return 唯一id
     */
    public static Long generateAccountId() {
        return idWorkerForAccount.nextId();
    }
    /**
     * 静态工具类
     * 获取Tokenid
     *
     * @return 唯一id
     */
    public static Long generateTokenId() {
        return idWorkerForToken.nextId();
    }

    //==============================Constructors=====================================
    /**
     * 构造函数
     * @param workerId 工作ID (0~31)
     * @param dataCenterId 数据中心ID (0~31)
     */
    public SnowflakeIdWorker(long workerId, long dataCenterId) {
        //对wordId和dataCenterId校验保证正确性，每个有位数限制
        if (workerId > maxWorkerId || workerId < 0) {
            throw new IllegalArgumentException(String.format("workerId can't be greater than %d or less than 0", maxWorkerId));
        }
        if (dataCenterId > maxDataCenterId || dataCenterId < 0) {
            throw new IllegalArgumentException(String.format("dataCenterId can't be greater than %d or less than 0", maxDataCenterId));
        }
        this.workerId = workerId;
        this.dataCenterId = dataCenterId;
    }

    // ==============================Methods==========================================
    /**
     * 获得下一个ID (该方法是线程安全的)
     * @return SnowflakeId
     */
    public synchronized long nextId() {
        //得到当前的时间戳
        long timestamp = timeGen();

        //如果当前时间小于上一次ID生成的时间戳，说明系统时钟回退过这个时候应当抛出异常
        if (timestamp < lastTimestamp) {
            throw new RuntimeException(
                    String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", lastTimestamp - timestamp));
        }

        //如果是同一时间生成的，则进行毫秒内序列
        if (lastTimestamp == timestamp) {
            sequence = (sequence + 1) & sequenceMask;
            //毫秒内序列溢出
            if (sequence == 0) {
                //阻塞到下一个毫秒,获得新的时间戳
                timestamp = tilNextMillis(lastTimestamp);
            }
        }
        //时间戳改变，毫秒内序列重置
        else {
            sequence = 0L;
        }

        //上次生成ID的时间截
        lastTimestamp = timestamp;

        //   移位并通过或运算拼到一起组成64位的ID
        //   最高位是时间 左移22位，右边目前有22个0
        //   接下来是数据中心id 左移17位 右边目前还有17个0 数据中心本身占5位 不会影响时间
        //   接下来是线程id 左移12位 右边目前还有12个0 线程id本身占5位 不会影响数据中心id
        //   最后是序列值 最后占12位
        //   左移运算规则: 1<<2=100;(二进制运算)
        //   与运算运算规则：0|0=0；  0|1=1；  1|0=1；   1|1=1；
        //
        //   示例
        //   timestamp        101<<12 = 101 0000 0000 0000
        //   dataCenterId     1001<<8 =     1001 0000 0000
        //   workerId         1010<<4 =          1010 0000
        //   sequence         1       =                  1
        //   结果                      = 101 1001 1010 0001
        return ((timestamp - twepoch) << timestampLeftShift)
                | (dataCenterId << dataCenterIdShift)
                | (workerId << workerIdShift)
                | sequence;
    }

    /**
     * 阻塞到下一个毫秒，直到获得新的时间戳
     *
     * @param lastTimestamp 上次生成ID的时间截
     * @return 当前时间戳
     */
    protected long tilNextMillis(long lastTimestamp) {
        long timestamp = timeGen();
        while (timestamp <= lastTimestamp) {
            timestamp = timeGen();
        }
        return timestamp;
    }

    /**
     * 返回以毫秒为单位的当前时间
     *
     * @return 当前时间(毫秒)
     */
    protected long timeGen() {
        return System.currentTimeMillis();
    }

    /**
     * 模拟获取进程id
     * 分布式情况下每多一个进程相当于多了一倍的id数量
     *
     * @return 进程id
     */
    private static Long getWorkId() {
        try {
            String hostAddress = Inet4Address.getLocalHost().getHostAddress();
            int[] ints = StringUtils.toCodePoints(hostAddress);
            int sums = 0;
            for (int b : ints) {
                sums += b;
            }
            return (long) (sums % 32);
        } catch (UnknownHostException e) {
            // 如果获取失败，则使用随机数备用
            return RandomUtils.nextLong(0, 31);
        }
    }

    /**
     * 模拟获取数据中心ID
     * 分布式情况下每多一个数据中心相当于多了一倍的id数量
     * 不加 tryCatch 方法会报错，不知原因，抄 getWorkId() 方法的兜底策略
     *
     * @return 数据中心id
     */
    private static Long getDataCenterId() {
        try {
            int[] ints = StringUtils.toCodePoints(SystemUtils.getHostName());
            int sums = 0;
            for (int i : ints) {
                sums += i;
            }
            return (long) (sums % 32);
        } catch (Exception e) {
            // 如果获取失败，则使用随机数备用
            return RandomUtils.nextLong(0, 31);
        }
    }

}


