package com.example.mylogin.common.utils;

import com.example.mylogin.common.exception.MyException;
import com.example.mylogin.enums.ResponseEnum;

import java.util.Date;

/**
 * =======================================================
 *
 * @company 技术中心-中台技术部-共享平台组
 * @className DateUtils
 * @description 时间工具类，时间处理操作
 * @date 2021/10/5 8:39 下午
 * @author liwenjie
 * @version 0.1
 * =======================================================
 */
public class DateUtils {

    /**
     * @className DateUtils
     * @description 比较两个日期的差是否大于指定间隔时间
     *  如果大于，返回true
     *  否则返回false
     *  如果其中有时间为空，直接返回true
     * @date 2021/10/5 9:15 下午
     * @param firstDate 第一个时间
	 * @param secondDate 第二个时间
	 * @param timeType 时间单位
	 * @param timeSub 时间值
     * @return boolean
     * @author liwenjie
     * @jarversion 0.0.1
     */
    public static boolean compareTwoDateGteTheSub(Date firstDate, Date secondDate, TimeSubTypeEnum timeType, int timeSub){
        if(firstDate == null || secondDate == null) return true;
        long abs = Math.abs(firstDate.getTime() - secondDate.getTime());
        int sub = timeType.getTypeSub() * timeSub;
        return abs > sub;
    }

    /**
     * @className DateUtils
     * @description 根据type获取时间差值类型枚举类
     * @date 2021/10/5 9:43 下午
     * @param type 类型值
     * @return com.example.mylogin.utils.DateUtils.TimeSubTypeEnum
     * @author liwenjie
     * @jarversion 0.0.1
     */
    public static TimeSubTypeEnum getTimeSubTypeEnumByType(int type){
        TimeSubTypeEnum[] values = TimeSubTypeEnum.values();
        for (TimeSubTypeEnum value : values) {
            if(value.getType() == type) return value;
        }
        throw new MyException(ResponseEnum.TIME_SUB_TYPE_PARAM_ERROR);
    }
    /**
     * =======================================================
     *
     * @company 技术中心-中台技术部-共享平台组
     * @className DateUtils
     * @description 时间差值类型枚举类，比较时间间隔时使用
     * @date 2021/10/5 9:33 下午
     * @author liwenjie
     * @version 0.0.1
     * =======================================================
     */
    public enum TimeSubTypeEnum{
        SECOND(1,1000,"秒"),
        MINUTE(2,60 * 1000,"分钟"),
        HOUR(3,60 * 60 * 1000,"小时"),
        DAY(4,24 * 60 * 60 * 1000,"天"),
        ;
        private int type;
        private int typeSub;
        private String desc;

        public int getType() {
            return type;
        }

        public int getTypeSub() {
            return typeSub;
        }

        public String getDesc() {
            return desc;
        }

        TimeSubTypeEnum(int type, int typeSub, String desc) {
            this.type = type;
            this.typeSub = typeSub;
            this.desc = desc;
        }
    }
}
