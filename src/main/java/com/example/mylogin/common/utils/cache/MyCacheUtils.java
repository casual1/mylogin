package com.example.mylogin.common.utils.cache;

import com.alibaba.fastjson.JSONObject;
import com.example.mylogin.common.utils.DesUtils;
import com.example.mylogin.db.AccountInfo;
import com.example.mylogin.db.ProductInfo;
import com.example.mylogin.db.Token;
import com.example.mylogin.entity.TokenSInfo;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 缓存操作类
 * 对外暴露的缓存操作类，所有操作缓存统一经过这里
 * 如果缓存源切换，只在此类做修改即可
 * 继承 MyCache 类操作缓存
 * 单例模式
 */
public class MyCacheUtils extends MyCache {
    /**
     * 私有化构造器
     */
    private MyCacheUtils() {
    }

    /**
     * 根据账户名获取账户id
     * 逻辑
     *  根据账户名获取id
     *  如果id为空返回-1
     *  否则返回正常id
     * @param accountName 账户名
     * @return 账户id
     */
    public long getAccountIdByAccountName(String accountName) {
        Long id = accountNameIdCache.get(accountName);
        if(id==null) return -1L;
        return id;
    }

    /**
     * 根据账户id获取账户信息
     * @param id 账户ID
     * @return 账户信息
     */
    public AccountInfo getAccountInfoById(long id) {
        return accountInfoMap.get(id);
    }

    /**
     * 插入用户信息
     * @param accountInfo 账户信息
     */
    public void addAccount(AccountInfo accountInfo) {
        accountInfoMap.put(accountInfo.getId(),accountInfo);
        accountNameIdCache.put(accountInfo.getAccountName(),accountInfo.getId());
    }

    /**
     * 保存token信息
     * 持久化的token信息
     * @param token token
     */
    public void addToken(Token token) {
        tokenCache.put(token.getId(),token);
    }

    /**
     * 根据id获取token
     * @param tokenId tokenId
     * @return Token
     */
    public Token getTokenById(long tokenId) {
        return tokenCache.get(tokenId);
    }

    /**
     * 根据id和state获取token
     * @param tokenId 主键
     * @param state 数据状态
     * @return token对象
     */
    public Token getTokenByIdAndState(long tokenId, int state) {
        Token token = tokenCache.get(tokenId);
        if(token.getState() == state)
            return token;
        return null;
    }

    /**
     * 根据id修改token的state信息
     * @param tokenId 主键
     * @param state 数据状态
     */
    public void updateTokenStateById(long tokenId, int state){
        Token token = tokenCache.get(tokenId);
        if(token == null || token.getState() == state) return;
        token.setState(state);
        token.setLogoutTime(new Date());
        tokenCache.put(tokenId,token);
    }

    /**
     * 保存token信息 Map<Long,Map<Long,Token>> accountId tokenId token
     * 此方法存放登录状态校验的缓存信息
     * 跟持久化的数据库数据做区分
     * @param token 创建成功的token对象
     */
    public void addVerifyToken(Token token) {
        Map<Long, Token> accountIdMap = verifyTokenCache.get(token.getAccountId());
        //如果不存在accountId，新建内层map
        if(CollectionUtils.isEmpty(accountIdMap)){
            accountIdMap = new ConcurrentHashMap<>();
        }
        //存放tokenId和token
        accountIdMap.put(token.getId(),token);
        //存放账户id和时间戳map
        verifyTokenCache.put(token.getAccountId(),accountIdMap);
    }

    /**
     * 保存根据登陆时间排序的token令牌
     * @param verifyToken token对象
     */
    public void addTreeMapVerifyToken(Token verifyToken) {
        treeMapVerifyTokenCache.put(verifyToken.getLoginTime(),verifyToken.getTokenS());
    }

    /**
     * 获取第一个根据登陆时间排序的token令牌
     */
    public TokenSInfo getFirstTreeMapVerifyToken() {
        Iterator<Map.Entry<Date, String>> iterator = treeMapVerifyTokenCache.entrySet().iterator();
        if(iterator.hasNext()){
            return JSONObject.parseObject(DesUtils.getDecryptString(iterator.next().getValue()),TokenSInfo.class);
        }
        return null;
    }

    /**
     * 获取所有的缓存token
     * @return Map<Long,Map<Long, Token>>
     */
    public Map<Long,Map<Long, Token>> getAllVerifyToken(){
        return verifyTokenCache;
    }

    /**
     * 先根据accountId获取，然后根据time获取
     * 此方法获取登录状态校验的缓存信息
     * @param tokenSInfo 前端的tokeninfo
     * @return Token
     */
    public Token getVerifyToken(TokenSInfo tokenSInfo) {
        Map<Long, Token> longTokenMap = verifyTokenCache.get(tokenSInfo.getAccountId());
        if(CollectionUtils.isEmpty(longTokenMap))
            return null;
        return longTokenMap.get(tokenSInfo.getTokenId());
    }

    /**
     * 删除缓存中的token信息
     * @param accountId 账户id
     * @param tokenId tokenid
     */
    public void delVerifyToken(long accountId, long tokenId){
        Map<Long, Token> longTokenMap = verifyTokenCache.get(accountId);
        // 删除失效的token
        if(!CollectionUtils.isEmpty(longTokenMap)) {
            longTokenMap.remove(tokenId);
        }
        // 如果用户没有登录状态的数据，直接删除此账户的key
        if(CollectionUtils.isEmpty(longTokenMap)){
            verifyTokenCache.remove(accountId);
        }
    }

    /**
     * 保存产品信息
     * @param productInfo 产品信息
     */
    public void addProduct(ProductInfo productInfo) {
        productInfoMap.put(productInfo.getId(), productInfo);
        productNameIdCache.put(productInfo.getName(), productInfo.getId());
    }

    /**
     * 根据id获取产品信息
     * @param id 主键id
     * @return 产品信息
     */
    public ProductInfo getProductById(long id) {
        return productInfoMap.get(id);
    }

    /**
     * 根据产品名称获取产品主键id
     * @param productName 产品名称
     * @return 主键
     */
    public long getProductIdByProductName(String productName) {
        Long id = productNameIdCache.get(productName);
        if(id==null) return -1L;
        return id;
    }

    /**
     * 内部类实现单例
     */
    private static class Instance{
        private static final MyCacheUtils myCacheUtils = new MyCacheUtils();
    }

    /**
     * 获取单例对象方法
     * @return 工具类对象
     */
    public static MyCacheUtils getInstance(){
        return Instance.myCacheUtils;
    }


}

