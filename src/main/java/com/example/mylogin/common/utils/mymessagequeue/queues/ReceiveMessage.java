package com.example.mylogin.common.utils.mymessagequeue.queues;

/**
 * =======================================================
 * @className ReceiveClient
 * @description 消费消息接口，承载消息的处理方法对象
 * @author liwenjie
 * @date 2021/11/21 5:55 下午
 * =======================================================
 */
@FunctionalInterface
public interface ReceiveMessage {

    /**
     * 消费消息的方法，因为每个业务方对消息的处理方式不同，业务方需要自己实现
     * @param message 消息
     */
    void receiveMessage(String message);
}
