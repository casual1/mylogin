package com.example.mylogin.common.utils.mymessagequeue;


import com.example.mylogin.common.utils.mymessagequeue.queues.MyMessageQueue;
import com.example.mylogin.common.utils.mymessagequeue.queues.MyMessageQueueUtils;
import com.example.mylogin.common.utils.mymessagequeue.queues.ReceiveMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * =======================================================
 *
 * @author liwenjie
 * @className MyReceiveClient
 * @description 接收客户端 测试不同的client可以接收相同的消息
 * @date 2021/11/20 10:44 下午
 * =======================================================
 */
public class MyLogoutMessageReceiveOtherClient implements ReceiveMessage {

    private final static Logger logger = LoggerFactory.getLogger(MyLogoutMessageReceiveOtherClient.class);

    private static final String topic = "user_logout";
    private static final Integer clientId = 2;


    //  在执行方法前初始化
    static {
        logger.info("-- MyLogoutMessageReceiveOtherClient static initializer --");
        MyMessageQueueUtils.getInstance().regist(topic, MyMessageQueue.ClientType.RECEIVE.getType(), clientId);
    }

    public static void receive() {
        try {
            MyMessageQueueUtils.getInstance().receiveMessage(topic, clientId, new MyLogoutMessageReceiveOtherClient());
        } catch (Exception e) {
            String format = String.format("-- MyLogoutMessageReceiveOtherClient receive exception --,topic:%s,clientId:%s", topic, clientId);
            logger.error(format, e);
        }
    }


    /**
     * 处理消息的方法
     *
     * @param message 消息
     */
    @Override
    public void receiveMessage(String message) {
        logger.info("-- MyLogoutMessageReceiveOtherClient receiveMessage --,message:{}", message);
    }
}
