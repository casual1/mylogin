package com.example.mylogin.common.utils.mymessagequeue.queues;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

/**
 * =======================================================
 * @className MyMessageQueueUtils
 * @description 消息发送工具类
 * @author liwenjie
 * @date 2021/11/20 10:43 下午
 * =======================================================
 */
public class MyMessageQueueUtils extends MyMessageQueue {

    private final static Logger logger = LoggerFactory.getLogger(MyMessageQueueUtils.class);

    private MyMessageQueueUtils() {
    }

    private static class MyMessageQueueUtilsInstance {
        private static final MyMessageQueueUtils myMessageQueueUtils = new MyMessageQueueUtils();
    }

    public static MyMessageQueueUtils getInstance() {
        return MyMessageQueueUtilsInstance.myMessageQueueUtils;
    }


    /**
     * 服务注册
     *
     * @param topic    主题
     * @param clientId clientId
     */
    public void regist(String topic, Integer clientTupe, Integer clientId) {
        if (!client.containsKey(topic)) {
            //  如果主题不存在
            //  新建发送方和消费方的的 Map<Integer, Set<Integer>>
            Map<Integer, Set<Integer>> map = new ConcurrentHashMap<>();
            Set<Integer> set =  new CopyOnWriteArraySet<>();
            map.put(ClientType.RECEIVE.getType(), set);
            map.put(ClientType.SEND.getType(), set);
            client.put(topic, map);
        }
        //  将相应的clientId放入注册信息
        client.get(topic).get(clientTupe).add(clientId);
        logger.info("-- MyMessageQueueUtils regist do client success--,topic:{},clientType:{},clientId:{}",topic,clientTupe,clientId);
        if(ClientType.RECEIVE.getType() == clientTupe){
            if (!queueMap.containsKey(topic)) {
                //  如果主题不存在
                //  新建载体Map 放入对应的 clientId 和 队列
                Map<Integer,LinkedBlockingQueue<String>> blockingQueueMap = new ConcurrentHashMap<>();
                blockingQueueMap.put(clientId,new LinkedBlockingQueue<>());
                queueMap.put(topic,blockingQueueMap);
            } else {
                //  直接放入对应的 clientId 和 队列
                queueMap.get(topic).put(clientId, new LinkedBlockingQueue<>());
            }
            logger.info("-- MyMessageQueueUtils regist do queueMap success--,topic:{},clientType:{},clientId:{}",topic,clientTupe,clientId);
        }
    }

    /**
     * 发送消息 此处使用消息超时的方法，防止被阻塞
     * @param topic     主题
     * @param clientId  clientId
     * @param message   消息体
     */
    public void sendMessage(String topic, Integer clientId, String message) throws InterruptedException {
        //  开始发送数据每一个client发送一份
        Map<Integer, Set<Integer>> integerListMap = client.get(topic);
        if(integerListMap!=null && !integerListMap.isEmpty()) {
            Set<Integer> integers = integerListMap.get(ClientType.RECEIVE.getType());
            for (Integer integer : integers) {
                if(queueMap.get(topic).containsKey(integer)){
                    queueMap.get(topic).get(integer).offer(message,3, TimeUnit.SECONDS);
                }
            }
        }
        logger.info("-- MyMessageQueueUtils sendMessage success --,topic:{},clientId:{},message:{}",topic,clientId,message);
    }

    /**
     * 消费消息
     * @param topic 主题
     * @param clientId clientId
     * @param receiveMessage 消费消息的方法
     * @throws Exception 获取队列数据异常
     */
    public void receiveMessage(String topic, Integer clientId, ReceiveMessage receiveMessage) throws Exception{
        if(queueMap.containsKey(topic) && queueMap.get(topic).containsKey(clientId)){
            LinkedBlockingQueue<String> messageQueue = queueMap.get(topic).get(clientId);
            new Thread(new ReceiveMQTask(topic, clientId, messageQueue, receiveMessage)).start();

        }
    }

    /**
     * =======================================================
     * @className MyMessageQueueUtils
     * @description 消费MQ的线程
     * @author liwenjie
     * @date 2021/11/28 1:02 下午
     * =======================================================
     */
    private class ReceiveMQTask implements Runnable{

        private String topic;
        private Integer clientId;
        private LinkedBlockingQueue<String> messageQueue;
        private ReceiveMessage receiveMessage;

        public ReceiveMQTask(String topic, Integer clientId, LinkedBlockingQueue<String> messageQueue, ReceiveMessage receiveMessage) {
            this.topic = topic;
            this.clientId = clientId;
            this.messageQueue = messageQueue;
            this.receiveMessage = receiveMessage;
        }

        @Override
        public void run() {
            while (true){
                try {
                    String message = messageQueue.take();
                    if(ObjectUtils.isNotEmpty(message)){
                        receiveMessage.receiveMessage(message);
                    }
                } catch (InterruptedException e) {
                    String format = String.format("-- ReceiveMQTask run exception --,topic:%s,clientId:%s",topic,clientId);
                    logger.error(format,e);
                }

            }
        }
    }

}
