package com.example.mylogin.common.utils;

import com.example.mylogin.enums.ResponseEnum;
import com.example.mylogin.common.exception.MyException;
import com.example.mylogin.vo.ResponseResultVO;

/**
 * 结果结果封装工具
 */
public class ResponseUtils {


    /**
     * 成功无返回值
     * @return ResponseResultVO
     */
    public static <T> ResponseResultVO<T> ok() {
        return new ResponseResultVO<>(ResponseEnum.OK.getCode(),ResponseEnum.OK.getMsg());
    }

    /**
     * 成功并且有返回值
     * @return ResponseResultVO
     */
    public static <T> ResponseResultVO<T> ok(T data) {
        ResponseResultVO<T> responseResultVO = new ResponseResultVO<>(ResponseEnum.OK.getCode(), ResponseEnum.OK.getMsg());
        responseResultVO.setData(data);
        return responseResultVO;
    }

    /**
     * 自定义提示
     * @return ResponseResultVO
     */
    public static <T> ResponseResultVO<T> newResponse(ResponseEnum responseEnum) {
        return new ResponseResultVO<>(responseEnum.getCode(),responseEnum.getMsg());
    }

    /**
     * 自定义提示
     * 第一个参数code码
     * 第二个参数是错误信息
     * @return ResponseResultVO
     */
    public static <T> ResponseResultVO<T> newResponse(Integer code,String msg) {
        return new ResponseResultVO<>(code,msg);
    }

    /**
     * 自定义异常提示
     * @return ResponseResultVO
     */
    public static <T> ResponseResultVO<T> myException(MyException myException) {
        return new ResponseResultVO<>(myException.getCode(),myException.getMsg());
    }

    /**
     * 未知异常提示
     * @return ResponseResultVO
     */
    public static <T> ResponseResultVO<T> error() {
        return new ResponseResultVO<>(ResponseEnum.ERROR.getCode(),ResponseEnum.ERROR.getMsg());
    }
}
