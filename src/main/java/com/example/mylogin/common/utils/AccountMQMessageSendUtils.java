package com.example.mylogin.common.utils;

import com.example.mylogin.common.utils.mymessagequeue.MyLogoutMessageSendClient;
import com.example.mylogin.db.Token;
import com.example.mylogin.entity.AccountMQMessage;

/**
 * =======================================================
 * @className AccountMQMessageSendUtils
 * @description 账号信息MQ发送工具类
 * @author liwenjie
 * @date 2021/11/22 9:00 下午
 * =======================================================
 */
public class AccountMQMessageSendUtils {

    /**
     * 发送退出登录的MQ消息
     * @param token 登出的token信息
     */
    public static void sendLogoutMQ(Token token){
        MyLogoutMessageSendClient.sent(AccountMQMessage.build().tokenLogout(token).toJSON());
    }
}
