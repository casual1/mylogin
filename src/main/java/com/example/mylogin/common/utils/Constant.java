package com.example.mylogin.common.utils;

/**
 * =======================================================
 *
 * @company 技术中心-中台技术部-共享平台组
 * @className Constant
 * @description 常量信息存储类
 * @date 2021/10/5 8:34 下午
 * @author liwenjie
 * @version 0.1
 * =======================================================
 */
public class Constant {

    /**
     * 登录token的过期时间数值 10
     */
    public static int TOKEN_EXPIRATION_TIME = 5;
    public static int TOKEN_EXPIRATION_TIME_TYPE = DateUtils.TimeSubTypeEnum.MINUTE.getType();
}
