package com.example.mylogin.common.utils.mymessagequeue;

import com.example.mylogin.common.utils.mymessagequeue.queues.MyMessageQueue;
import com.example.mylogin.common.utils.mymessagequeue.queues.MyMessageQueueUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * =======================================================
 * @className MySendClient
 * @description 发送客户端
 * @author liwenjie
 * @date 2021/11/20 10:44 下午
 * =======================================================
 */
public class MyLogoutMessageSendClient{

    private final static Logger logger = LoggerFactory.getLogger(MyLogoutMessageSendClient.class);

    private static final String topic = "user_logout";
    private static final Integer clientId = 1;

    /**
     * 初始化方法
     */
    public static void init() {
        logger.info("-- MyLogoutMessageSendClient init --");
        MyMessageQueueUtils.getInstance().regist(topic, MyMessageQueue.ClientType.SEND.getType(), clientId);
    }

    /**
     * 发送消息
     * @param message 消息内容
     */
    public static void sent(String message){
        try {
            MyMessageQueueUtils.getInstance().sendMessage(topic,clientId,message);
        } catch (InterruptedException e) {
            String format = String.format("-- MyLogoutMessageSendClient sent exception --,topic:%s,clientId:%s,message:%s",topic,clientId,message);
            logger.error(format,e);
        }
    }

}
