package com.example.mylogin.common.utils.mymessagequeue.queues;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * =======================================================
 * @className MyMessageQueue
 * @description 模拟发送MQ消息的接口信息
 *  消息队列主要的使用属性
 *  topic 消息主题 标定不同的功能使用不同的主题
 *  clientId 发送方/消费方的服务，一个topic可以有多个消费方和发送方，要保证所有的消费方都能消费到消息
 *  message 消息体，需要交互的消息
 *  本次主要实现两个功能
 *   1、可以根据不同的topic区分消息
 *   2、同一topic下不同的消费方client都可以消费到同一份消息
 * @author liwenjie
 * @date 2021/11/20 10:44 下午
 * =======================================================
 */
public class MyMessageQueue {



    /**
     * 保存消息的载体
     * 设计思路：
     *  String 存储相应的topic
     *  Map<Integer,LinkedBlockingQueue> 保存消费方的clientId和需要消费的消息
     *  Integer 消费方clientid
     *  LinkedBlockingQueue 消息队列
     * 原因：
     *  需要保证发送方发送的消息需要被所有的消费方client消费到，所以每存在一个消费方的client就需要一份消息副本
     */
    protected static final Map<String,Map<Integer,LinkedBlockingQueue<String>>> queueMap = new ConcurrentHashMap<>();

    /**
     * 存放注册信息
     * 客户端需要注册自己的topic和clientId
     * 如果消费方不注册，发送方不知道是否需要发送消息给对应的client
     * 设计思路：
     *  String 存储相应的topic
     *  Map<Integer, List<Integer>> 保存此topic的发送方和消费方的client
     *  Integer 存储是发送方和消费方的类型
     *  List<Integer> 存储发送方或者消费方有几个clientId
     * 原因：
     *  保存消息的载体需要根据注册信息新建或者删除队列
     */
    protected static final Map<String,Map<Integer, Set<Integer>>> client = new ConcurrentHashMap<>();

    /**
     * =======================================================
     * @company 技术中心-中台技术部-共享平台组
     * @className MyMessageQueue
     * @description 服务类型枚举 发送方和消费方
     * @date 2021/11/20 9:52 下午
     * @author liwenjie
     * @version 0.0.1
     * =======================================================
     */
    public enum ClientType{
        SEND(1,"发送"),
        RECEIVE(2,"接收"),
        ;
        private int type;
        private String desc;

        public int getType() {
            return type;
        }

        public String getDesc() {
            return desc;
        }

        ClientType(int type, String desc) {
            this.type = type;
            this.desc = desc;
        }
    }
}
