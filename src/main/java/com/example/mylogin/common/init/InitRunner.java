package com.example.mylogin.common.init;

import com.example.mylogin.common.utils.mymessagequeue.MyLogoutMessageReceiveClient;
import com.example.mylogin.common.utils.mymessagequeue.MyLogoutMessageReceiveOtherClient;
import com.example.mylogin.common.utils.mymessagequeue.MyLogoutMessageSendClient;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * =======================================================
 * @className InitRunner
 * @description 初始化相关方法
 * @author liwenjie
 * @date 2021/11/22 8:38 下午
 * =======================================================
 */
@Component
public class InitRunner implements CommandLineRunner {

    @Override
    public void run(String... args) {
        MyLogoutMessageSendClient.init();
        MyLogoutMessageReceiveClient.receive();
        MyLogoutMessageReceiveOtherClient.receive();
    }
}
