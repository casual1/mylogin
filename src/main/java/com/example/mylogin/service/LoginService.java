package com.example.mylogin.service;

import com.example.mylogin.entity.TokenSInfo;
import com.example.mylogin.vo.LoginParam;

import java.util.Map;

/**
 * 登录服务接口
 */
public interface LoginService {

    /**
     * 注册功能
     * @param loginParam 账户名密码等相关信息
     * @return 账户id
     */
    long regist(LoginParam loginParam);

    /**
     * 账户名密码登录
     * @param loginParam 账户名密码等相关信息
     * @return Map<String,Object> 保存登录信息
     */
    Map<String,Object> accountNameLogin(LoginParam loginParam);

    /**
     * 校验token是否有效
     * @param loginParam token相关信息
     * @return Map<String, Object> token校验结果
     */
    Map<String, Object> verifyToken(LoginParam loginParam);

    /**
     *  失效此token的登录状态
     *    1、删除缓存中的token信息
     *    2、修改持久化token种的数据状态
     * @param accountId 账号id
     * @param tokenId tokenid
     */
    void invalidLoginInformation(long accountId, long tokenId);
}
