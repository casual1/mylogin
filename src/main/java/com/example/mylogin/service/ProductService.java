package com.example.mylogin.service;

import com.example.mylogin.db.ProductInfo;

/**
 * 产品信息操作接口
 */
public interface ProductService {
    /**
     * 增加产品接口
     * @param productName 产品名称
     * @return 产品信息id
     */
    long addProduct(String productName);

    /**
     * 根据id查询产品
     * @param product 产品id
     * @return 产品信息
     */
    ProductInfo getById(int product);
}
