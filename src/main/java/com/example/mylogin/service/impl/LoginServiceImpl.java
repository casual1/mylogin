package com.example.mylogin.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.mylogin.common.exception.MyException;
import com.example.mylogin.common.utils.*;
import com.example.mylogin.common.utils.cache.MyCacheUtils;
import com.example.mylogin.dao.AccountInfoDao;
import com.example.mylogin.dao.TokenInfoDao;
import com.example.mylogin.db.AccountInfo;
import com.example.mylogin.entity.AccountMQMessage;
import com.example.mylogin.enums.ResponseEnum;
import com.example.mylogin.enums.TokenStateEnum;
import com.example.mylogin.vo.LoginParam;
import com.example.mylogin.db.Token;
import com.example.mylogin.entity.TokenSInfo;
import com.example.mylogin.service.LoginService;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 登录服务实现
 */
@Service
public class LoginServiceImpl implements LoginService {

    private static final Logger logger =  LoggerFactory.getLogger(LoginServiceImpl.class);

    @Autowired
    private AccountInfoDao accountInfoDao;

    @Autowired
    private TokenInfoDao tokenInfoDao;


    /**
     * 注册方法实现
     * 逻辑
     *  判断用户名是否存在
     *      存在提示 账户名已经存在
     *      不存在则创建用户
     * 增加锁防止并发问题
     *  双端检索防止进入锁等待之后唤醒的线程未判断用户而直接覆盖数据
     * 锁对象使用账户名是因为不会影响其他的账户注册
     * @param loginParam 账户名密码等相关信息
     * @return 账户id
     */
    @Override
    public long regist(LoginParam loginParam) {
        String accountName = loginParam.getAccountName();
        //根据账户名查找
        AccountInfo accountInfo = accountInfoDao.getByAccountName(accountName);
        if(accountInfo==null){
            //加锁
            synchronized (accountName){
                //双重检索
                accountInfo = accountInfoDao.getByAccountName(accountName);
                if(accountInfo==null){
                    //注册用户
                    return accountInfoDao.add(new AccountInfo(loginParam));
                }
            }
        }
        throw new MyException(ResponseEnum.ACCOUNT_IS_EXIST);
    }

    /**
     * 账户名密码登录方式实现
     * 逻辑
     *  判断用户名是否存在
     *      如果不存在，返回失败
     *  判断密码是否一致
     *      如果不一致，返回失败
     *  第一版
     *      生成登录状态 token
     *      保存token
     *      将token转为json对象
     *      加密转化后的json对象
     *      返回数据
     *  第二版
     *      生成需要返回给前端的tokenSInfo对象
     *      将tokenSInfo对象转JSONString，加密
     *      生成Token对象，将加密的TokenS存入属性
     *      token对象存入缓存
     *      生成一份用与登录校验的缓存
     *  第三版
     *      借助id生成器
     *      生成tokenSInfo对象（账户id，tokenId：id生成器生成，登录时间）
     *      将tokenSInfo对象转JSONString，加密
     *      设置token的id为tokenSInfo的tokenId，id生成器生成不用担心重复
     *      token对象存入缓存
     * @param loginParam 账户名密码等相关信息
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> accountNameLogin(LoginParam loginParam) {
        String accountName = loginParam.getAccountName();
        String password = loginParam.getPassword();
        //验证账户名是否存在
        AccountInfo byAccountName = accountInfoDao.getByAccountName(accountName);
        if(byAccountName==null) throw new MyException(ResponseEnum.ACCOUNT_IS_NOT_EXIST);
        //验证密码是否一致
        if(!StringUtils.equals(password,byAccountName.getPassword())) throw new MyException(ResponseEnum.ACCOUNT_PASSWORD_ERROR);
        //登录时间
        Date loginTime = new Date();
        //生成tokenId
        long tokenId = SnowflakeIdWorker.generateTokenId();
        //返回前端的tokenS
        TokenSInfo tokenSInfo = new TokenSInfo(tokenId ,byAccountName.getId(), loginTime);
        //加密
        String encryptString;
        try {
            encryptString = DesUtils.getEncryptString(JSON.toJSONString(tokenSInfo));
        } catch (Exception e) {
            logger.error(String.format("--LoginServiceImpl accountNameLogin 加密tokenS error--,loginParam:%s",loginParam), e);
            throw new MyException(ResponseEnum.ENCRYPT_TOKEN_ERROR);
        }
        if(StringUtils.isBlank(encryptString))
            throw new MyException(ResponseEnum.ENCRYPT_TOKEN_ERROR);
        //封装需要入库保存的token信息
        Token token = new Token(tokenId,byAccountName.getId(),loginTime,encryptString, loginParam);
        long id = tokenInfoDao.add(token);
        //成功返回结果
        Map<String, Object> result = new HashMap<>();
        if(id>0) {
            result.put("code", "1");
            result.put("meg", "登录成功");
            result.put("token", encryptString);
            return result;
        }
        result.put("code", "-1");
        result.put("meg", "登录失败");
        return result;
    }

    /**
     * 校验token是否有效
     * 逻辑
     *  判断token是否存在
     *      如果不存在，返回失败
     *  如果存在校验tokenS和缓存中token的tokenS是否一致
     *      如果不一致，可能是同时间登录被重写了
     * @param loginParam token相关信息
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> verifyToken(LoginParam loginParam) {
        //拿到加密的token
        String tokenS = loginParam.getTokenS();
        //解密
        TokenSInfo tokenSInfo;
        try {
            tokenSInfo = JSONObject.parseObject(DesUtils.getDecryptString(tokenS), TokenSInfo.class);
        } catch (Exception e) {
            throw new MyException(ResponseEnum.DECRYPT_TOKEN_ERROR);
        }
        //验证解密后的参数是否完整
        if(tokenSInfo==null || tokenSInfo.getAccountId()<=0L || tokenSInfo.getLoginTime() == null)
            throw new MyException(ResponseEnum.DECRYPT_TOKEN_ERROR);
        //判断登录是否过期
        if(DateUtils.compareTwoDateGteTheSub(new Date(),tokenSInfo.getLoginTime(), DateUtils.getTimeSubTypeEnumByType(Constant.TOKEN_EXPIRATION_TIME_TYPE),Constant.TOKEN_EXPIRATION_TIME)){
            //失效登录信息
            invalidLoginInformation(tokenSInfo.getAccountId(),tokenSInfo.getTokenId());
            throw new MyException(ResponseEnum.VERIFY_TOKEN_EXPIRATION);
        }
        //看缓存中是否存在对应的token数据
        Token verifyToken = MyCacheUtils.getInstance().getVerifyToken(tokenSInfo);
        if(verifyToken==null){
            logger.info("--LoginServiceImpl verifyToken 缓存中不存在--");
            //数据库中获取有效数据
            verifyToken = tokenInfoDao.getTokenByIdAndState(tokenSInfo.getTokenId(), TokenStateEnum.valid.getState());
            if (verifyToken != null){
                //写入缓存
                MyCacheUtils.getInstance().addVerifyToken(verifyToken);
                //根据时间排序的Token信息，TreeMap
                MyCacheUtils.getInstance().addTreeMapVerifyToken(verifyToken);
            }
        }
        //没有获取到token
        if(verifyToken==null){
            //失效登录信息
            invalidLoginInformation(tokenSInfo.getAccountId(),tokenSInfo.getTokenId());
            throw new MyException(ResponseEnum.VERIFY_TOKEN_ERROR);
        }
        //校验过期token是否过期
        if(DateUtils.compareTwoDateGteTheSub(new Date(),verifyToken.getLoginTime(), DateUtils.getTimeSubTypeEnumByType(Constant.TOKEN_EXPIRATION_TIME_TYPE),Constant.TOKEN_EXPIRATION_TIME)){
            //失效登录信息
            invalidLoginInformation(tokenSInfo.getAccountId(),tokenSInfo.getTokenId());
            throw new MyException(ResponseEnum.VERIFY_TOKEN_EXPIRATION);
        }
        Map<String, Object> result = new HashMap<>();
        result.put("code", "1");
        result.put("meg", "校验成功");
        result.put("accountId",verifyToken.getAccountId());
        return result;
    }

    /**
     * 删除缓存中的token信息
     * 修改持久化token的数据状态
     *  增加了数据状态，获取的时候需要对token的状态进行判断，只获取有效的数据
     * @param accountId 账户id
     * @param tokenId tokenid
     */
    @Override
    public void invalidLoginInformation(long accountId, long tokenId) {
        //删除缓存数据
        MyCacheUtils.getInstance().delVerifyToken(accountId, tokenId);
        //修改持久化数据状态
        MyCacheUtils.getInstance().updateTokenStateById(tokenId, TokenStateEnum.invalid.getState());
        //发送MQ消息，暂时使用队列代替
        Token tokenById = tokenInfoDao.getTokenById(tokenId);
        if(ObjectUtils.isEmpty(tokenById))
            return;
        AccountMQMessageSendUtils.sendLogoutMQ(tokenById);
    }
}
