package com.example.mylogin.service.impl;

import com.example.mylogin.dao.ProductDao;
import com.example.mylogin.db.ProductInfo;
import com.example.mylogin.enums.ResponseEnum;
import com.example.mylogin.common.exception.MyException;
import com.example.mylogin.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 产品信息操作接口实现
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    /**
     * 新增产品信息
     * 逻辑
     *  判断名称是否存在
     *      存在给出提示
     *      不存在插入新的产品信息
     * @param productName 产品名称
     * @return 产品id
     */
    @Override
    public long addProduct(String productName) {
        ProductInfo productInfo = productDao.getByProductName(productName);
        if(productInfo!=null) throw new MyException(ResponseEnum.PRODUCT_IS_EXIST);
        return productDao.add(new ProductInfo(productName));
    }

    /**
     * 根据id获取产品信息
     * @param productId 产品id
     * @return 产品信息
     */
    @Override
    public ProductInfo getById(int productId) {
        return productDao.getByProductId(productId);
    }
}
