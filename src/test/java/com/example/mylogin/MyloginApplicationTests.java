package com.example.mylogin;

import com.example.mylogin.controller.LoginController;
import com.example.mylogin.controller.ProductController;
import com.example.mylogin.enums.ResponseEnum;
import com.example.mylogin.vo.LoginParam;
import com.example.mylogin.vo.ResponseResultVO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * 登录接口测试
 */
@SpringBootTest
class MyloginApplicationTests {

    private static final Logger logger =  LoggerFactory.getLogger(MyloginApplicationTests.class);

    @Autowired
    private LoginController loginController;

    @Autowired
    private ProductController productController;
    /**
     * 模拟账户名称
     */
    private static final String[] accountNameArray = {"zhangsan", "lisi", "wangwu", "zhaoliu"};
    /**
     * 密码默认账户名+123
     */
    private static final String password = "123";
    /**
     * 模拟账户名称
     */
    private static final String[] productNameArray = {"饿了么", "美团", "大众点评"};

    private static final List<String> tokenSList = new LinkedList<>();

    /**
     * 未来的测试接口需要先注册成功用户数据然后才可以进行模拟登录的过程
     * 先增加产品信息
     *  注册需要检验产品信息
     * 然后进行用户注册
     */
    @BeforeEach
    public void init(){
        logger.info("--MyloginApplicationTests init 开始新增产品--");
        for (String productName : productNameArray) {
            productController.addProduct(productName);
        }
        logger.info("--MyloginApplicationTests init 新增产品结束--");
        logger.info("----------------------------------------------------");
        logger.info("--MyloginApplicationTests init 开始注册用户--");
        for (String accountName : accountNameArray) {
            loginController.regist(new LoginParam().name(accountName).pwd(accountName + password).platform(1).product(1));
        }
        logger.info("--MyloginApplicationTests init 注册用户结束--");
        logger.info("----------------------------------------------------");
        logger.info("--MyloginApplicationTests init 开始用户登录--");
        for (String accountName : accountNameArray) {
            ResponseResultVO<Map<String, Object>> login = loginController.accountNameLogin(new LoginParam().name(accountName).pwd(accountName + password).platform(1).product(1));
            if(login.getCode() == ResponseEnum.OK.getCode()){
                tokenSList.add(login.getData().get("token").toString());
            }
        }
        logger.info("--MyloginApplicationTests init 用户登录结束--");
        logger.info("----------------------------------------------------");
        logger.info("--MyloginApplicationTests init 模拟登录校验开始--");
        for (String s : tokenSList) {
            loginController.verifyToken(new LoginParam().tokenS(s));
        }
        logger.info("--MyloginApplicationTests init 模拟登录校验结束--");
        logger.info("----------------------------------------------------");

    }
    
    @Test
    public void test(){
        logger.info("-- MyloginApplicationTests test --");
    }

    /**
     * 测试登录状态失效
     * @throws InterruptedException 中断异常
     */
    @Test
    public void testVerifyToken() throws InterruptedException {
        ResponseResultVO<Map<String,Object>> responseResultVO;
        int num = 0;
        do {
            num++;
            logger.info("----------------------------------------------------");
            TimeUnit.SECONDS.sleep(5);
            for (String s : tokenSList) {
                responseResultVO = loginController.verifyToken(new LoginParam().tokenS(s));
                logger.info("--MyloginApplicationTests testVerifyToken again--,result:{}", responseResultVO);
            }
        } while (num <= 6);
    }

    /**
     * 测试账户名登录接口 新增平台和产品信息
     */
    @Test
    public void testAccountNameLoginWithPlatformAndProduct(){
        ResponseResultVO<Map<String,Object>> responseResultVO = loginController.accountNameLogin(new LoginParam()
                                                                                .name(accountNameArray[0])
                                                                                .pwd(accountNameArray[0] + password)
                                                                                .platform(1)
                                                                                .product(2));
        logger.info("--MyloginApplicationTests testAccountNameLoginWithPlatformAndProduct--,responseResultVO:{}",responseResultVO);
    }

    /**
     * 测试返回给前端加密的token信息
     */
    @Test
    public void testGetAccountNameLoginResult(){
        ResponseResultVO<Map<String,Object>> responseResultVO;
        for (String accountName : accountNameArray) {
            responseResultVO = loginController.accountNameLogin(new LoginParam(accountName, accountName + password));
            logger.info("--MyloginApplicationTests testGetAccountNameLoginResult--,responseResultVO:{}",responseResultVO);
        }
    }

    /**
     * 测量批量登录的接口，虽然保存token是增加了时间戳的区分，
     * 只能保证单线程用户不同时间的登录状态
     * 并发情况下同一毫秒内同一用户的登录状态是是会被覆盖
     * 结果比较登录成功返回的token的登录时间即可
     * @throws InterruptedException 并发异常
     */
    @Test
    public void testConcurrentAccountNameLogin() throws InterruptedException {
        //10个线程后一起执行
        CyclicBarrier cyclicBarrier = new CyclicBarrier(10);
        //线程池，非必需
        Executor executor = Executors.newFixedThreadPool(10);
        for(int i = 0; i < 10; i++) {
            executor.execute(new LoginTask(cyclicBarrier,""+i+"号线程",i));
        }
        //主线程需要等其他线程结束，要不然看不到其他的日志打印
        TimeUnit.SECONDS.sleep(20);
    }

    /**
     * 登录线程模拟并发登录
     */
    public class LoginTask implements Runnable{

        private CyclicBarrier cyclicBarrier;//并发工具类
        private String name;//区分不同线程
        private int arriveTime;//模拟不同的时间
        private String action = "登录";

        public LoginTask(CyclicBarrier cyclicBarrier,String name,int arriveTime){
            this.cyclicBarrier = cyclicBarrier;
            this.name = name;
            this.arriveTime = arriveTime;
        }

        @Override
        public void run() {
            try {
                //模拟到达需要花的时间
                Thread.sleep(arriveTime * 1000L);
                System.out.println(name +"等待"+action);
                //每次来一个线程减1，到达设定的线程数量（本次是10）所有线程一起执行
                cyclicBarrier.await();
                //一起执行的操作
                ResponseResultVO<Map<String,Object>> regist = loginController.accountNameLogin(new LoginParam(accountNameArray[0], accountNameArray[0]+password));
                System.out.println(name+action+"结束，结果："+regist);
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 测试登录状态相关接口
     */
    @Test
    public void testAcountNameLoginState(){
        ResponseResultVO<Map<String,Object>> responseResultVO = loginController.accountNameLogin(new LoginParam(accountNameArray[0], accountNameArray[0] + password));
        logger.info("--MyloginApplicationTests testAcountNameLoginState--,responseResultVO:{}",responseResultVO);
    }

    /**
     * 测试登录接口
     */
    @Test
    public void testAccountNameLogin(){
        String accountName = "zhaojing";
        String password = "123";
        String errorword = "1";
        //登录
        ResponseResultVO<Map<String,Object>> responseResultVO = loginController.accountNameLogin(new LoginParam(accountName, password));
        logger.info("--MyloginApplicationTests testAccountNameLogin--,responseResultVO:{}",responseResultVO);
        //如果提示用户不存在则注册
        if(responseResultVO.getCode()==ResponseEnum.ACCOUNT_IS_NOT_EXIST.getCode()) {
            ResponseResultVO<Long> loginResultVo = loginController.regist(new LoginParam(accountName, password));
            logger.info("--MyloginApplicationTests testAccountNameLogin--,responseResultVO:{}",responseResultVO);
            //注册成功再次登录，错误密码
            if(loginResultVo.getCode()==ResponseEnum.OK.getCode()){
                responseResultVO = loginController.accountNameLogin(new LoginParam(accountName, password));
                logger.info("--MyloginApplicationTests testAccountNameLogin--,responseResultVO:{}",responseResultVO);
                //如果提示密码错误则使用正确密码登录
                if(responseResultVO.getCode()==ResponseEnum.ACCOUNT_PASSWORD_ERROR.getCode()){
                    responseResultVO = loginController.accountNameLogin(new LoginParam(accountName, password));
                    logger.info("--MyloginApplicationTests testAccountNameLogin--,responseResultVO:{}",responseResultVO);
                }
            }
        }

    }

    /**
     * 测试注册接口
     * 借用CyclicBarrier模拟等待10个线程一起执行
     * CyclicBarrier：https://www.jianshu.com/p/4ef4bbf01811
     * 可以暴露不使用锁会产生的问题
     *
     * 正常结果应该是只有一条成功，其他返回账户名称已经存在
     */
    @Test
    public void testRegist() throws InterruptedException {
        //10个线程后一起执行
        CyclicBarrier cyclicBarrier = new CyclicBarrier(10);
        //线程池，非必需
        Executor executor = Executors.newFixedThreadPool(10);
        for(int i = 0; i < 10; i++) {
            executor.execute(new RegistTask(cyclicBarrier,""+i+"号线程",i));
        }

        //主线程需要等其他线程结束，要不然看不到其他的日志打印
        TimeUnit.SECONDS.sleep(20);
    }


    /**
     * 注册线程，模拟并发注册
     */
    public class RegistTask implements Runnable{

        private CyclicBarrier cyclicBarrier;//并发工具类
        private String name;//区分不同线程
        private int arriveTime;//模拟不同的时间
        private String action = "注册";

        public RegistTask(CyclicBarrier cyclicBarrier,String name,int arriveTime){
            this.cyclicBarrier = cyclicBarrier;
            this.name = name;
            this.arriveTime = arriveTime;
        }

        @Override
        public void run() {
            try {
                //模拟到达需要花的时间
                Thread.sleep(arriveTime * 1000L);
                System.out.println(name +"等待"+action);
                //每次来一个线程减1，到达设定的线程数量（本次是10）所有线程一起执行
                cyclicBarrier.await();
                //一起执行的操作
                ResponseResultVO<Long> regist = loginController.regist(new LoginParam(name, name+password));
                System.out.println(name+action+"结束，结果："+regist);
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }

}
