package com.example.mylogin;


public class MyTest {

    public static void main(String[] args) {

        System.out.println(XMLUtils.build()
                .getXmlWithOneParam("${1}", "1")
                .getXmlWithOneParam("${2}", "2")
                .getXmlWithOneParam("${3}", "3")
                .getXmlWithOneParam("${4}", "4")
                .getResult()
        );

    }

}
