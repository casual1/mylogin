package com.example.mylogin;

import com.example.mylogin.common.utils.DesUtils;

/**
 * =======================================================
 * @company 技术中心-中台技术部-共享平台组
 * @className DesTests
 * @description 加解密测试
 * @date 2021/10/17 12:49 下午
 * @author liwenjie
 * @version 0.0.1
 * =======================================================
 */

public class DesTests {

    public static void main(String[] args) {
        String str = "{\"accountId\":609600832143306752,\"loginTime\":1634451777478,\"tokenId\":609600844866727936}";
        String decryptString = DesUtils.getEncryptString(str);
        System.out.println(decryptString);
        decryptString = "tokenS=i9TdbXTU3Wtv1gMRCe5b6pQTdaNlYj3gLEmX+c9OygfFy1nSIyESQPogDBbhSPqs5LwtMKsvNU2TV8rov3enWUrQ4235ZvLJ2PovLHQeQjS48eCkNJerDw==";
        String decryptString1 = DesUtils.getDecryptString(decryptString);
        System.out.println(decryptString1);

    }
}
