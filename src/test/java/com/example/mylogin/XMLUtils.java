package com.example.mylogin;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class XMLUtils {

    //模板字符串，只能被初始化，不能修改
    private final static String xmlTemplate;
    //最后输出结果
    private String xml;

    public XMLUtils(String xml) {
        this.xml = xml;
    }

    //加载文件到变量
    static {
        List<String> strings = new LinkedList<>();
        try {
            strings = Files.readAllLines(Paths.get("/Users/liwenjie/IdeaWorkSpace/mylogin/mylogin/src/main/resources/xml.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        xmlTemplate = String.join("\n",strings);
    }

    public static XMLUtils build(){
        return new XMLUtils(xmlTemplate);
    }

    public XMLUtils getXmlWithOneParam(String key, String param){
        this.xml = StringUtils.replace(this.xml,key,param);
        return this;
    }

    public String getResult(){
        return this.xml;
    }
}
