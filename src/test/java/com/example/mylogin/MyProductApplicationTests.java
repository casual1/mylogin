package com.example.mylogin;

import com.example.mylogin.controller.ProductController;
import com.example.mylogin.vo.ResponseResultVO;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 产品接口测试
 */
@SpringBootTest
public class MyProductApplicationTests {

    private static final Logger logger =  LoggerFactory.getLogger(MyProductApplicationTests.class);

    @Autowired
    private ProductController productController;

    /**
     * 测试添加
     * 肯定会有并发问题，不做测试
     */
    @Test
    public void testAddProduct(){
        ResponseResultVO responseResultVO = productController.addProduct("饿了吗");
        logger.info("--MyProductApplicationTests testAddProduct--,responseResultVO:{}",responseResultVO);

    }



}
